const axios = require('axios');
const cheerio = require('cheerio');
const {URL} = require('url');

const allowedHosts = ['reddit.com']


module.exports = {
    async startUp() {
        await Promise.all(allowedHosts.map(host => 
            createTask('fetchUrl', {url: `http://${host}`})
        ))
    },
    async fetchUrl(payload) {
        console.log(JSON.stringify({dataset, repository, owner}))
        console.log(`Recevied url payload ${JSON.stringify(payload)}`);

        const {url} = payload;

        await insert({
            url
        });
        
        const {data} = await axios.get(url);
        const $ = cheerio.load(data);

        const urls = [];
        $('a[href]').each(function () {
            const href = $(this).attr('href');
            const newUrl = new URL(href, url);
            if(newUrl.protocol.startsWith('http') && allowedHosts.includes(newUrl.host)){
                urls.push(newUrl.href);
            }
        });

        console.log(`Found ${urls.length} urls`)

        await Promise.all(urls.map(async url => createTask('fetchUrl', {url}, 2)));

        console.log('done')

    }
}